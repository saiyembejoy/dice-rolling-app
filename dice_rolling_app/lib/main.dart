import 'package:dice_rolling_app/gradient_container.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    const MaterialApp(
      home: Scaffold(
        body: GradientContainer(
          Color.fromARGB(255, 5, 86, 226),
          Color.fromARGB(255, 140, 52, 241),
        ),
      ),
      debugShowCheckedModeBanner: true,
    ),
  );
}
